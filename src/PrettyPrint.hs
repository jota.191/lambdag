{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE FlexibleContexts  #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE TypeFamilies #-}

{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE NoMonomorphismRestriction #-}
{-# LANGUAGE DataKinds #-}

{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeApplications #-}

module PrettyPrint where


import AST

import Language.Grammars.AspectAG
import Language.Grammars.AspectAG.TH


$(attLabel "ppN" ''String)

asp_ppNaive
  =    syn ppN p_Var (ter ch_varName)
  .+:  syn ppN p_App (ppAppN <$> at ch_appL ppN <*> at ch_appR ppN)
  .+:  syn ppN p_Abs (ppAbsN <$> ter ch_absName <*> at ch_absBody ppN)
  .+: emptyAspect


ppAppN l r = "("++ l ++")("++ r ++ ")"
ppAbsN v t = "λ" ++ v ++ "→" ++ t


pPrintNaive :: Term -> String
pPrintNaive e = sem_Term asp_ppNaive e emptyAtt #. ppN


$(attLabel "pp" ''String)
asp_ppAux
  =   syn pp p_Var (ter ch_varName)
  .+: syn pp p_App (
  do lpp <- at ch_appL pp
     rpp <- at ch_appR pp
     l   <- at ch_appL sid
     r   <- at ch_appR sid
     case l of
       App _ _ -> case r of
                    App _ _ -> return (lpp ++ paren rpp)
                    Abs _ _ -> return (lpp ++ paren rpp)
                    _       -> return (lpp ++ rpp)
       Abs _ _ -> return (paren lpp ++ paren rpp)
       Var x -> case r of
                 Var _ -> return (x ++ rpp)
                 _     -> return (x ++ paren rpp)
    )
    .+: syn pp p_Abs (
  do x <- ter ch_absName
     tpp <- at ch_absBody pp
     t   <- at ch_absBody sid
     case t of
       Abs _ _ -> return ("λ" ++ x ++ " " ++ tail tpp)
       _       -> return ("λ" ++ x ++ " → " ++ tpp)
    )
    .+: emptyAspect

asp_pp = asp_ppAux .:+: asp_sid

paren a = "(" ++ a ++ ")"


pPrint :: Term -> String
pPrint e = sem_Term asp_pp e emptyAtt #. pp
