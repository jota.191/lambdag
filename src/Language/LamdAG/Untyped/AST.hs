{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE FlexibleContexts  #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE TypeFamilies #-}

{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE NoMonomorphismRestriction #-}
{-# LANGUAGE DataKinds #-}

{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeApplications #-}

module Language.LambdAG.Untyped.AST where

import Language.Grammars.AspectAG
import Language.Grammars.AspectAG.TH

$(addNont "Term")
$(addProd "Var" ''Nt_Term [("varName", Ter ''String)])
$(addProd "App" ''Nt_Term [("appL", NonTer ''Nt_Term),
                           ("appR", NonTer ''Nt_Term)])
$(addProd "Abs" ''Nt_Term [("absName", Ter ''String),
                           ("absBody", NonTer ''Nt_Term)])

$(closeNT ''Nt_Term)
$(mkSemFunc ''Nt_Term)

