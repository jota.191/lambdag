{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE FlexibleContexts  #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE TypeFamilies #-}

{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE NoMonomorphismRestriction #-}
{-# LANGUAGE DataKinds #-}

{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeApplications #-}

module PrettyPrint where


import AST

import Language.Grammars.AspectAG
import Language.Grammars.AspectAG.TH

import Data.Set as S
type VarSet = S.Set String


$(attLabel "fvars" ''VarSet)

asp_fvars
  =   syn fvars p_Var (pure S.singleton <*> ter ch_varName)
  .+: syn fvars p_App (S.union <$> at ch_appL fvars <*> at ch_appR fvars)
  .+: syn fvars p_Abs (S.delete <$> ter ch_absName <*> at ch_absBody fvars)
  .+: emptyAspect



getFree :: Term -> VarSet
getFree t = sem_Term asp_fvars t emptyAtt #. fvars
