{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE FlexibleContexts  #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE TypeFamilies #-}

{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE NoMonomorphismRestriction #-}
{-# LANGUAGE DataKinds #-}

{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeApplications #-}

module AST where

import Language.Grammars.AspectAG
import Language.Grammars.AspectAG.TH


-- | The Grammar
$(addNont "Term")
$(addProd "Var" ''Nt_Term [("varName", Ter ''String)])
$(addProd "App" ''Nt_Term [("appL", NonTer ''Nt_Term),
                           ("appR", NonTer ''Nt_Term)])
$(addProd "Abs" ''Nt_Term [("absName", Ter ''String),
                           ("absBody", NonTer ''Nt_Term)])

$(closeNT ''Nt_Term)
$(mkSemFunc ''Nt_Term)



-- | example expressions
k = Abs "x" (Abs "y" (Var "x"))
i = Abs "x" (Var "x")
s = Abs "x" (Abs "y" (Abs "z" (App (App (Var "x")(Var "z"))
                               (App (Var "y") (Var "z")))))

-- | Church Numerals
church n = Abs "f" (Abs "x" (churchAux n))
  where churchAux 0 = Var "x"
        churchAux n = App (Var "f") $ churchAux (n-1)


-- | Identity Aspect
$(attLabel "sid" ''Term)

asp_sid
   =  syn sid p_Var (Var <$> ter ch_varName)
  .+: syn sid p_App (App <$> at ch_appL sid <*> at ch_appR sid)
  .+: syn sid p_Abs (Abs <$> ter ch_absName <*> at ch_absBody sid)
  .+: emptyAspect


ident t = sem_Term asp_sid t emptyAtt #. sid
